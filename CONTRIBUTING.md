### Communication
Use slack `ufaldsg` channel `dias-e2e` or create issue. You will get faster responses on slack if you are internal developer and have access.

#### Issues
- If you have a question, file an issue with the label `question`
- If you want to start discussion, file an issue with the label `discussion`
- Please be clear and give enough context when submitting an issue.

### Code development
Please keep these rules in mind during collaboration
1. Be nice to others!
2. The tasks with the highest priority are the most welcome!
3. Submit your contribution for code review via Merge Request (MR) to @oplatek or @tuetschek
    - It helps readibility by maintaining single style, prevents bugs
    - It documents decisions and new features
4. Prefer readibility.
5. Prefer smaller incremental frequent updates.
    - Use small commits
    - Scope as small features/tasks as possible for each MR.
6. Master brach should contain only working code.

### License

By contributing to dias-e2e you agree to publish your code under Apache 2.0 license.
If you are not allowed to license your contribution under Apache 2.0 license say it in MR,
but such contribution will not be allowed in most cases.